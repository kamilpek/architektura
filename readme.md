## Architektura Komputerów - 2015/2016
### Zadania

* Zadanie 1. - Zamiana liczb całkowitych zapisanych w systemie dziesiętnym na ich kod U2(32-bitowy);
* Zadanie 2. - Zamiana liczb zmiennoprzecinkowych w systemie dziesiętnym na ich reprezentację w formacie IEEE 754 na 32-bitach lub 64-bitach;

---
(C)Kamil Pek - 2016
